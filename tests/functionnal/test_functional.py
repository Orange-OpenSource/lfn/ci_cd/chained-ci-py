#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from chainedci.config import getConfig
from chainedci.log import init_log, log
from chainedci.scenario import Scenario
from chainedci.step import Step
import pytest
from tests.tests_lib import force_ini_values

log.setLevel('DEBUG')


def load_config():
    force_ini_values()
    ini = getConfig('ini')
    init_log(ini)
    c = getConfig('chainedci')
    c.clean_vault_keys()
    c.set_vault_key(ini['env']['TEST_VAULT_KEY'])
    c.load_file('tests/functionnal/inventory/host_vars/scenario_A.yml',
                'scenarios.a')
    c.load_file('tests/functionnal/inventory/group_vars/all.yml',
                'defaults')
    run = getConfig('run')
    run['name'] = 'a'
    run['job'] = 'project1'


def load_config_local():
    force_ini_values()
    ini = getConfig('ini')
    init_log(ini)
    c = getConfig('chainedci')
    c.clean_vault_keys()
    c.set_vault_key(ini['env']['TEST_VAULT_KEY'])
    c.load_file('tests/functionnal/inventory/host_vars/scenario_A.yml',
                'scenarios.a')
    c.load_file('tests/functionnal/inventory/group_vars/all_local_config.yml',
                'defaults')
    run = getConfig('run')
    run['name'] = 'a'
    run['job'] = 'project1'


ini = getConfig('ini')
'''
Init.
'''


@pytest.mark.skipif(not ini['env'].eval('TEST_VAULT_KEY'),
                    reason="skipping functionnal tests "
                           "(no TEST_VAULT_KEY var)")
def test_functionnal_sc_type():
    load_config()
    s = Scenario('a')
    assert isinstance(s, dict) is True


@pytest.mark.skipif(not ini['env'].eval('TEST_VAULT_KEY'),
                    reason="skipping functionnal tests "
                           "(no TEST_VAULT_KEY var)")
def test_functionnal_sc_type_local():
    load_config_local()
    s = Scenario('a')
    assert isinstance(s, dict) is True


'''
Get steps.
'''


@pytest.mark.skipif(not ini['env'].eval('TEST_VAULT_KEY'),
                    reason="skipping functionnal tests "
                           "(no TEST_VAULT_KEY var)")
def test_functionnal_sc_no_scenario():
    load_config()
    with pytest.raises(AttributeError):
        Scenario('empty_scenario')


@pytest.mark.skipif(not ini['env'].eval('TEST_VAULT_KEY'),
                    reason="skipping functionnal tests "
                           "(no TEST_VAULT_KEY var)")
def test_functionnal_sc_steps():
    load_config()
    s = Scenario('a')
    assert isinstance(list(s.steps.values())[0], Step) is True
