#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import pytest
from chainedci.env_vars import EnvVars


def test_env_vars_type():
    """Test class type."""
    env_vars = EnvVars()
    assert isinstance(env_vars, dict) is True


def test_config_env_vars_home():
    """Test access to a classic environment variable."""
    env_vars = EnvVars()
    assert isinstance(env_vars['HOME'], str) is True


@pytest.mark.parametrize("condition, result",
    [
        pytest.param('VAR', True, id='present'),
        pytest.param('NOTVAR', False, id='present2'),
        pytest.param('VAR == VAL', True, id='equal'),
        pytest.param('VAR == VAL2', False, id='equal2'),
        pytest.param('VAR2 == VAL2', False, id='equal_novar'),
        pytest.param('VAR==VAL', True, id='equal_space1'),
        pytest.param('VAR ==VAL', True, id='equal_space2'),
        pytest.param('VAR== VAL', True, id='equal_space3'),
        pytest.param('VAR != VAL2', True, id='not_equal'),
        pytest.param('VAR != VAL', False, id='not_equal2'),
        pytest.param('VAR2 != VAL', False, id='not_equal_novar'),
        pytest.param('VAR!=VAL2', True, id='not_equal_space1'),
        pytest.param('VAR!=VAL2', True, id='not_equal_space2'),
        pytest.param('VAR in [VAL]', True, id='in'),
        pytest.param('VAR2 in [VAL]', False, id='in_novar'),
        pytest.param('VAR in [VAL, VAL2, VAL3]', True, id='in2'),
        pytest.param('VAR in [VAL2, VAL3]', False, id='in3'),
        pytest.param('VAR in [ VAL ]', True, id='in_space1'),
        pytest.param('VAR in [ VAL, VAL2 ]', True, id='in_space2'),
        pytest.param('VAR in [ VAL ,VAL2 ]', True, id='in_space3'),
        pytest.param('VAR in [VAL ,VAL2]', True, id='in_space4'),
    ])
def test_config_env_parser(condition, result):
    env_vars = EnvVars()
    env_vars['VAR'] = 'VAL'
    assert env_vars.eval(condition) == result

def test_config_env_parser_unknown(caplog):
    env_vars = EnvVars()
    assert env_vars.eval("VAR > 5") == False
    assert "condition not parsed" in caplog.text
