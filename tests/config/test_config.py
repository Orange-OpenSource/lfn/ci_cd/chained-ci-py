#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

"""Test Config class."""

from ansible.parsing.yaml.objects import AnsibleVaultEncryptedUnicode
from ansible.parsing.vault import VaultLib
from ansible.errors import AnsibleFileNotFound
from chainedci.config import Config, getConfig, init_ini, simple_dict
from chainedci.log import log
import pytest

log.setLevel('DEBUG')


def test_config_type():
    """Test class type."""
    config = Config()
    assert isinstance(config, dict) is True


def test_config_with_scenario_init():
    """Test class type."""
    config = Config(init_scenarios=True)
    assert config['defaults'] == None
    assert config['scenarios'] == {}


def test_get_new_config():
    """Test class type."""
    config = getConfig('foo')
    assert isinstance(config, dict) is True


def test_get_existing_config():
    """Test class type."""
    config = getConfig('foo')
    config['bar'] = 'xxx'
    config2 = getConfig('foo')
    assert isinstance(config2, dict) is True


def test_config_set_vault_key():
    """Test the vault key type when load with string."""
    config = Config()
    config.set_vault_key('fake')
    assert isinstance(config.get_vault_key(), VaultLib) is True


def test_config_load_vault_key():
    """Test the vault key type when load from file."""
    config = Config()
    config.load_vault_key('tests/config/config_example_vault_file')
    assert isinstance(config.get_vault_key(), VaultLib) is True


def test_config_load_vault_key_bad_file():
    """Test loading a unknown file."""
    config = Config()
    with pytest.raises(FileNotFoundError):
        config.load_vault_key('tests/config/config_example_vault_file_bad')


def test_config_load_file_yaml_no_vault():
    """Test to load a yaml file."""
    config = Config()
    config.load_file('tests/config/config_example.yml')
    assert config['key0'] == 'value0'


def test_config_load_file_yaml_no_vault_to_key():
    """Test to load a file to a specific key."""
    config = Config()
    config.load_file('tests/config/config_example.yml', 'config1')
    assert config['config1']['key0'] == 'value0'


def test_config_load_file_yaml_no_vault_to_scenario_key():
    """Test to load a file to a specific scenario key."""
    config = Config()
    config.load_file('tests/config/config_example.yml', 'scenarios.config1')
    assert config['scenarios']['config1']['key0'] == 'value0'


def test_config_load_file_yaml_no_vault_bad_file():
    """Test to load an unexisting file."""
    config = Config()
    with pytest.raises(AnsibleFileNotFound):
        config.load_file('tests/config/config_example_bad.yml')


def test_config_load_file_yaml_no_vault_with_vault_key():
    """Test to load a file without vaulted var."""
    config = Config()
    config.set_vault_key('fake')
    config.load_file('tests/config/config_example.yml')
    assert config['key0'] == 'value0'


def test_config_load_file_yaml_vaulted_with_key():
    """Test to load a file with with a vaulted value."""
    config = Config()
    config.set_vault_key('fake')
    config.load_file('tests/config/config_example_with_vault.yml')
    assert config['key'] == 'value'


def test_config_load_file_yaml_vaulted_without_key(caplog):
    """Test to load a file with with a vaulted value."""
    config = Config()
    config.clean_vault_keys()
    config.load_file('tests/config/config_example_with_vault.yml')
    assert "__VAULTED_VALUE__" in config['key']


def test_config_load_file_yaml_all_vaulted():
    """Test to load a vaulted file."""
    config = Config()
    config.set_vault_key('fake')
    config.load_file('tests/config/config_example_vaulted.yml')
    assert config['key0'] == 'value0'


CONTENT_WITH_VAULT = '''---
key0: value0
key: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          646231343064306436323431623065303461316631336430343965623433336232
          303934306233613632333863393439363333326132303835643063316562350a36
          323262393561376165616164646666336431373732383135373862336233313033
          3632393733346539616133633865306362306662306463383932663563360a3138
          373466646361626139326632393966656465383331343133303534656566
'''

CONTENT_NO_VAULT = '''---
key0: value0
key1: value1
'''


def test_config_load_content_yaml_no_vault():
    """Test to load a yaml string."""
    config = Config()
    config.load_content(CONTENT_NO_VAULT)
    assert config['key0'] == 'value0'


def test_config_load_content_yaml_no_vault_to_key():
    """Test to load a yaml string to a key."""
    config = Config()
    config.load_content(CONTENT_NO_VAULT, 'config1')
    assert config['config1']['key0'] == 'value0'


def test_config_load_content_yaml_no_vault_with_existing_key():
    """Test to update an existing key."""
    config = Config()
    config.load_content(CONTENT_NO_VAULT, 'config1')
    config.load_content("key0: valueX", 'config1')
    assert config['config1']['key0'] == 'valueX'


def test_config_load_content_yaml_no_vault_with_key():
    """Test to load a yaml string with a vault key set."""
    config = Config()
    config.set_vault_key('fake')
    config.load_content(CONTENT_NO_VAULT)
    assert config['key0'] == 'value0'


def test_config_load_content_yaml_vaulted_with_key():
    """Test to load a vaulted yaml string."""
    config = Config()
    config.set_vault_key('fake')
    config.load_content(CONTENT_WITH_VAULT)
    assert config['key'] == 'value'


def test_config_load_content_yaml_vaulted_with_key(caplog):
    """Test to load a vaulted yaml string."""
    config = Config()
    config.clean_vault_keys()
    config.load_content(CONTENT_WITH_VAULT)
    print(config['key0'])
    print(type(config['key']))
    assert config['key0'] == 'value0'
    assert "__VAULTED_VALUE__" in config['key']


def test_config_clean_vault_key():
    """Test to load a yaml string with a vault key set."""
    config = Config()
    config.set_vault_key('fake')
    config.clean_vault_keys()
    assert config.__class__.dl._vault.secrets is None


def test_config_simplify():
    config = Config()
    data = AnsibleVaultEncryptedUnicode('johndoe')
    config['user'] = data
    assert isinstance(config['user'], AnsibleVaultEncryptedUnicode)
    config.simplify()
    assert isinstance(config['user'], str)


def test_init_ini_without_user_config():
    init_ini(user_config_file='/unexisting_file/chainedci.yml')
    ini = getConfig('ini')
    assert list(ini.keys()) == ['env', 'log', 'artifacts', 'specific_steps',
                                'encryption', 'auth', 'timers', 'generator',
                                'init']


def test_init_ini_with_user_config():
    init_ini(user_config_file='tests/config/config_example.yml')
    ini = getConfig('ini')
    print(ini.keys())
    assert list(ini.keys()) == ['env', 'log', 'artifacts', 'specific_steps',
                                'encryption', 'auth', 'timers', 'generator',
                                'init', 'key0', 'key1']


def test_simple_dict():
    sample = {'key1': [1, 2, 3, 4],
              'key2': {'subkey1': 'aaa',
                       'subkey2': [1, 2, 3],
                       'subkey3': AnsibleVaultEncryptedUnicode('abcde')}}
    result = sample
    result['key2']['subkey3'] = 'abcde'
    assert simple_dict(sample) == result
