#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

'''
Test Artifact.
'''

from copy import deepcopy
from os import listdir
from shutil import copy, rmtree
from tempfile import mkdtemp, NamedTemporaryFile
from ansible.errors import AnsibleError
from ansible.parsing.vault import VaultEditor
import zipfile
import pytest
import schema
import base64
from chainedci.artifact import Artifact
from chainedci.log import log

log.setLevel('DEBUG')

ENCRYPT_DATA = [
    pytest.param({'local_files': [{'tests/artifacts/local_src.yml':
                                   'local_src1.yml'},
                                  {'tests/artifacts/local_src2.yml':
                                   'local_src2.yml'}],
                  'encrypt': True},
                 {},
                 [{'destination': 'local_src1.yml',
                   'source': 'tests/artifacts/local_src.yml',
                   'type': 'local'},
                  {'destination': 'local_src2.yml',
                   'source': 'tests/artifacts/local_src2.yml',
                   'type': 'local'}],
                 id="local_files")
]

SIMPLE_DATA = [
    pytest.param({'get_artifacts': 'config',
                  'encrypt': False},
                 {'branch': 'master'},
                 [{'type': 'pipeline', 'source': 'config'}],
                 id="artifact_get_str"),
    pytest.param({'get_artifacts': [{'name': 'stepX'}, {'name': 'stepY'}],
                  'encrypt': False},
                 {'branch': 'master'},
                 [{'type': 'pipeline', 'source': 'stepX'},
                  {'type': 'pipeline', 'source': 'stepY'}],
                 id="artifact_get_list"),
    pytest.param({'get_artifacts': [{'name': 'stepX', 'static_src': True}],
                  'encrypt': False},
                 {'branch': 'master'},
                 [{'type': 'local_archive',
                   'source': 'config/artifacts/stepX.zip'}],
                 id="artifact_get_local_static"),
    pytest.param({'get_artifacts': [{'name': 'stepX', 'static_src': True}],
                  'encrypt': False},
                 {'api': 'https://gitlabapi',
                  'branch': 'master',
                  'path': 'config_path'},
                 [{'type': 'remote_archive',
                   'source': 'https://gitlabapi/repository/files/config_path'
                             '/config/artifacts/stepX.zip?ref=master'}],
                 id="artifact_get_api_static"),
    pytest.param({'get_artifacts': [{'name': 'stepX', 'static_src': True}],
                  'encrypt': False},
                 {'url': 'https://myurl',
                  'branch': 'master',
                  'path': 'config_path'},
                 [{'type': 'remote_archive',
                   'source': 'https://myurl/raw/master/config_path'
                             '/config/artifacts/stepX.zip'}],
                 id="artifact_get_url_static"),
    pytest.param({'infra_pdfidf': 'myinfra',
                  'encrypt': False},
                 {'api': 'https://gitlabapi',
                  'branch': 'master',
                  'path': 'config_path'},
                 [{'type': 'remote',
                   'source': 'https://gitlabapi/repository/files/config_path'
                             '/config/myinfra.yaml?ref=master',
                   'destination': 'vars/pdf.yml'},
                  {'type': 'remote',
                   'source': 'https://gitlabapi/repository/files/config_path/'
                             'config/idf-myinfra.yaml?ref=master',
                   'destination': 'vars/idf.yml'}],
                 id="artifact_get_remote_api"),
    pytest.param({'remote_files': [{'url1': 'path1'}, {'url2': 'path2'}],
                  'encrypt': False},
                 {'branch': 'master'},
                 [{'type': 'remote', 'source': 'url1', 'destination': 'path1'},
                  {'type': 'remote', 'source': 'url2', 'destination': 'path2'}
                  ],
                 id="remote_files"),
    pytest.param({'certificates': 'certificates_url',
                  'encrypt': False},
                 {'branch': 'master'},
                 [{'type': 'local',
                   'destination': 'vars/certificates.yml',
                   'source': 'certificates/certificates_url'}],
                 id="certificates"),
    pytest.param({'ssh_creds': 'ssh_creds_url',
                  'encrypt': False},
                 {'branch': 'master'},
                 [{'type': 'local',
                   'destination': 'vars/vaulted_ssh_credentials.yml',
                   'source': 'ssh_creds/ssh_creds_url'}],
                 id="ssh_creds"),
    pytest.param({'ssh_access': 'ssh_access_url',
                  'encrypt': False},
                 {'branch': 'master'},
                 [{'type': 'local',
                   'destination': 'vars/ssh_gateways.yml',
                   'source': 'config/ssh_gateways/ssh_access_url'}],
                 id="ssh_access")
]

DEPRECATED_DATA = [

]

ALL = ENCRYPT_DATA + SIMPLE_DATA + DEPRECATED_DATA


class MockArtifactSrc(dict):
    def load(self, var1):
        return None


@pytest.mark.parametrize("artifact, config_source, result", ALL)
def test_artifact(artifact, config_source, result, mocker):
    # Mock the ArtifactSrc
    mocker.patch('chainedci.artifact.ArtifactSrc', MockArtifactSrc)
    artif = Artifact(artifact, 'stepX', config_source)
    # Check type
    assert isinstance(artif, dict) is True
    # Check we have N Src.name
    a_src = list(artifact.values())[0]
    if isinstance(a_src, list):
        assert len(artif.sources) == len(a_src)
    else:
        assert len(artif.sources) == 1 + (
            1 if 'infra_pdfidf' in artifact else 0)
    print(artif.sources)
    print(result)
    assert artif.sources == result


@pytest.mark.parametrize("artifact, config_source, result", deepcopy(ENCRYPT_DATA))
def test_artifact_bin(artifact, config_source, result, mocker):
    # Mock the ArtifactSrc
    mocker.patch('chainedci.artifact.ArtifactSrc', MockArtifactSrc)
    # Force encrypt to false
    artifact['encrypt'] = False
    artif = Artifact(artifact, 'stepY', config_source)
    # Load the Src and get the binary - We force here the content
    fake_folder = mkdtemp()
    mocker.patch('chainedci.artifact.mkdtemp', lambda: fake_folder)
    copy('tests/artifacts/local_src.yml', fake_folder)
    artif_bin = artif.bin
    # Create a new zip file and unzip it to compare content
    zip_file = NamedTemporaryFile(suffix='.zip')
    with open(zip_file.name, mode='wb') as file:
        file.write(base64.b64decode(artif_bin))
    zip_target = mkdtemp()
    with zipfile.ZipFile(zip_file, 'r') as zip_ref:
        zip_ref.extractall(zip_target)
    assert listdir(zip_target) == ['local_src.yml']
    rmtree(zip_target)
    zip_file.close()


@pytest.mark.parametrize("artifact, config_source, result", deepcopy(ENCRYPT_DATA))
def test_artifact_get_local(artifact, config_source, result, mocker):
    # Mock the ArtifactSrc
    mocker.patch('chainedci.artifact.ArtifactSrc', MockArtifactSrc)
    # Force encrypt to false
    artifact['encrypt'] = False
    artif = Artifact(artifact, 'stepY', config_source)
    # Load the Src and get the binary - We force here the content
    fake_folder = mkdtemp()
    mocker.patch('chainedci.artifact.mkdtemp', lambda: fake_folder)
    copy('tests/artifacts/local_src.yml', fake_folder)
    # Create a temp folder as 'local'
    local_folder = mkdtemp()
    artif.get_local(local_folder)
    # Check local contains the mock file
    assert listdir(local_folder) == ['local_src.yml']
    rmtree(local_folder)


def test_bad_artifact(mocker):
    mocker.patch('chainedci.artifact.ArtifactSrc', {})
    src = {'bad_kind': [{'aaa': 'xxx'}, {'bbb': 'yyy'}],
           'encrypt': False}
    with pytest.raises(schema.SchemaWrongKeyError):
        Artifact(src, 'stepX', {})


@pytest.mark.parametrize("artifact, config_source, result", deepcopy(ENCRYPT_DATA))
def test_artifact_encrypt(artifact, config_source, result, mocker):
    mocker.patch('chainedci.artifact.getConfig',
                 lambda x: (
                     {'key': 'testing'.encode("utf-8")} if x == 'run' else {
                      'encryption':
                          {'method': 'ansible_vault',
                           'key_env_name': 'ANSIBLE_VAULT_PASSWORD'}}))
    artif = Artifact(artifact, 'stepX', config_source)
    artif_bin = artif.bin
    # Create a new zip file and unzip it to compare content
    zip_file = NamedTemporaryFile(suffix='.zip')
    with open(zip_file.name, mode='wb') as file:
        file.write(base64.b64decode(artif_bin))
    zip_target = mkdtemp()
    with zipfile.ZipFile(zip_file, 'r') as zip_ref:
        zip_ref.extractall(zip_target)
    assert listdir(zip_target).sort() == [
        'local_src1.yml', 'local_src2.yml'].sort()
    with open(f"{zip_target}/local_src1.yml", mode='r') as file:
        content = file.read()
    assert content.startswith('$ANSIBLE_VAULT;1.1;AES256')
    rmtree(zip_target)
    zip_file.close()


@pytest.mark.parametrize("artifact, config_source, result", deepcopy(ENCRYPT_DATA))
def test_artifact_encrypt_no_vault_key(artifact, config_source, result, mocker):
    mocker.patch('chainedci.artifact.getConfig',
                 lambda x: (
                     {'env': {},
                      'encryption':
                          {'method': 'ansible_vault',
                           'key_env_name': 'ANSIBLE_VAULT_PASSWORD'}}))
    artif = Artifact(artifact, 'stepX', config_source)
    with pytest.raises(ValueError):
        artif.bin


@pytest.mark.parametrize("artifact, config_source, result", deepcopy(ENCRYPT_DATA))
def test_artifact_encrypt_bad_method(artifact, config_source, result, mocker):
    mocker.patch('chainedci.artifact.getConfig',
                 lambda x: (
                     {'env': {},
                      'encryption':
                          {'method': 'unknown_method',
                           'key_env_name': 'ANSIBLE_VAULT_PASSWORD'}}))
    artif = Artifact(artifact, 'stepX', config_source)
    with pytest.raises(ValueError):
        artif.bin

@pytest.mark.parametrize("artifact, config_source, result", deepcopy(ENCRYPT_DATA))
def test_artifact_already_encrypted(artifact, config_source, result,
                                    mocker, caplog):
    mocker.patch('chainedci.artifact.getConfig',
                 lambda x: (
                     {'env': {'ANSIBLE_VAULT_PASSWORD': 'testing'},
                      'encryption':
                          {'method': 'ansible_vault',
                           'key_env_name': 'ANSIBLE_VAULT_PASSWORD'}}))
    artif = Artifact(artifact, 'stepX', config_source)
    artif._load_vault_lib = mocker.Mock()
    artif.vault_ed = VaultEditor()
    artif.vault_ed.encrypt_file = mocker.Mock(
        side_effect=AnsibleError('input is already encrypted'))
    artif.bin
    assert 'is already encrypted, continue' in caplog.text

@pytest.mark.parametrize("artifact, config_source, result", deepcopy(ENCRYPT_DATA))
def test_artifact_encryption_crash(artifact, config_source, result,
                                   mocker, caplog):
    mocker.patch('chainedci.artifact.getConfig',
                 lambda x: (
                     {'env': {'ANSIBLE_VAULT_PASSWORD': 'testing'},
                      'encryption':
                          {'method': 'ansible_vault',
                           'key_env_name': 'ANSIBLE_VAULT_PASSWORD'}}))
    artif = Artifact(artifact, 'stepX', config_source)
    artif._load_vault_lib = mocker.Mock()
    artif.vault_ed = VaultEditor()
    artif.vault_ed.encrypt_file = mocker.Mock(
        side_effect=AnsibleError('another ansible error'))
    with pytest.raises(AnsibleError):
        artif.bin

@pytest.mark.parametrize("artifact, config_source, result", deepcopy(ENCRYPT_DATA))
def test_artifact_encryption_crash2(artifact, config_source, result,
                                    mocker, caplog):
    mocker.patch('chainedci.artifact.getConfig',
                 lambda x: (
                     {'env': {'ANSIBLE_VAULT_PASSWORD': 'testing'},
                      'encryption':
                          {'method': 'ansible_vault',
                           'key_env_name': 'ANSIBLE_VAULT_PASSWORD'}}))
    artif = Artifact(artifact, 'stepX', config_source)
    artif._load_vault_lib = mocker.Mock()
    artif.vault_ed = VaultEditor()
    artif.vault_ed.encrypt_file = mocker.Mock(
        side_effect=ValueError('another ansible error'))
    with pytest.raises(ValueError):
        artif.bin