#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from unittest.mock import MagicMock
import sys
import pytest
from chainedci.scenario import Scenario
from chainedci.config import Config, getConfig
from chainedci.log import log
from tests.tests_lib import force_ini_values

force_ini_values()
log.setLevel('DEBUG')

scenario_simple = {
    'jumphost': {
        'server': 'server1',
        'user': 'userA'
    },
    'scenario_steps': {
        'stepA': {'project': 'projectX'},
        'stepB': {'project': 'projectY'}
    }
}

config_all = {
    'stages': [
        'stage1',
        'stage2',
    ],
    'runner': {
        'tags': ['tag1', 'tag2'],
        'images': {},
        'env_vars': {'env1': 'value1', 'env2': 'value2'}
    },
    'gitlab': {
        'base_url': 'http://my.gitlab',
        'api_url': 'http://my.api.gitlab',
        'private_token': 'zorglubisstronger',
        'git_projects': {
            'projectX': {},
            'projectY': {},
        }
    },
}

config = Config(load_env=False)
config['defaults'] = config_all
config['scenarios'] = {'simple': scenario_simple}


def test_scenario_load(mocker, caplog):
    run_value = {'name': 'simple'}
    mocker.patch('chainedci.scenario.getConfig', lambda x: run_value)
    mocker.patch('chainedci.scenario.config', config)
    mocker.patch('chainedci.scenario.Step')
    scenario = Scenario('simple')
    assert isinstance(scenario, dict)
    assert len(scenario.steps) == len(scenario_simple['scenario_steps'])
    assert "loading step 'stepA'" in caplog.text
    assert "loading step 'stepB'" in caplog.text


def test_scenario_load_just_one(mocker, caplog):
    run_value = {'name': 'simple', 'job': 'stepB'}
    mocker.patch('chainedci.scenario.getConfig', lambda x: run_value)
    mocker.patch('chainedci.scenario.config', config)
    mocker.patch('chainedci.scenario.Step')
    scenario = Scenario('simple')
    assert isinstance(scenario, dict)
    assert len(scenario.steps) == 1
    assert "loading step 'stepA'" not in caplog.text
    assert "loading step 'stepB'" in caplog.text


def test_scenario_load_group_ssh_access(mocker, caplog):
    run_value = {'name': 'simple', 'job': 'stepA'}
    mocker.patch('chainedci.scenario.getConfig', lambda x: run_value)
    config['defaults']['ssh_access'] = 'ssh_access.eyml'
    mocker.patch('chainedci.scenario.config', config)
    mock_s = mocker.patch('chainedci.scenario.Step')
    scenario = Scenario('simple')
    assert "push group ssh_access 'stepA'" in caplog.text
    assert 'group_ssh_access' in mock_s.mock_calls[0].args[1]
    config['defaults'] = config_all


def test_scenario_load_group_certificate(mocker, caplog):
    run_value = {'name': 'simple', 'job': 'stepA'}
    mocker.patch('chainedci.scenario.getConfig', lambda x: run_value)
    config['defaults']['certificates'] = 'certificate.eyml'
    mocker.patch('chainedci.scenario.config', config)
    mock_s = mocker.patch('chainedci.scenario.Step')
    scenario = Scenario('simple')
    assert "push group certificates 'stepA'" in caplog.text
    assert 'group_certificates' in mock_s.mock_calls[0].args[1]
    config['defaults'] = config_all


def test_scenario_load_group_creds(mocker, caplog):
    run_value = {'name': 'simple', 'job': 'stepA'}
    mocker.patch('chainedci.scenario.getConfig', lambda x: run_value)
    config['defaults']['ansible_ssh_creds'] = 'creds.eyml'
    mocker.patch('chainedci.scenario.config', config)
    mock_s = mocker.patch('chainedci.scenario.Step')
    scenario = Scenario('simple')
    assert "push group ssh_creds 'stepA'" in caplog.text
    assert 'group_ssh_creds' in mock_s.mock_calls[0].args[1]
    config['defaults'] = config_all


step_result = [
    pytest.param("failed", 3, id='step_failed'),
    pytest.param("canceled", 2, id='step_canceled'),
    pytest.param("skipped", 1, id='step_skipped'),
    pytest.param("success", 0, id='step_succeded'),
]


@pytest.mark.parametrize("step_run_result, sc_run_result", step_result)
def test_scenario_run(step_run_result, sc_run_result, mocker):
    run_value = {'name': 'simple', 'job': 'stepA'}
    mocker.patch('chainedci.scenario.getConfig', lambda x: run_value)
    mocker.patch('chainedci.scenario.config', config)
    mocker.patch('chainedci.scenario.Step')
    scenario = Scenario('simple')
    scenario.steps['stepA'].run = lambda: step_run_result
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        scenario.run('stepA')
        scenario.steps['stepA'].run.assert_called_once()
    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == sc_run_result


def test_scenario_bad(mocker):
    run_value = {'name': 'simple'}
    mocker.patch('chainedci.scenario.getConfig', lambda x: run_value)
    mocker.patch('chainedci.scenario.config', config)
    mocker.patch('chainedci.scenario.Step')
    with pytest.raises(AttributeError):
        Scenario('unknown')


def test_scenario_run_bad_step(mocker):
    run_value = {'name': 'simple'}
    mocker.patch('chainedci.scenario.getConfig', lambda x: run_value)
    mocker.patch('chainedci.scenario.config', config)
    mocker.patch('chainedci.scenario.Step')
    scenario = Scenario('simple')
    with pytest.raises(ValueError):
        scenario.run('unknown_step')


def test_scenario_run_with_exception(mocker, caplog):
    run_value = {'name': 'simple', 'job': 'stepA'}
    mocker.patch('chainedci.scenario.getConfig', lambda x: run_value)
    mocker.patch('chainedci.scenario.config', config)
    mocker.patch('chainedci.scenario.Step')
    mocker.patch('sys.exit')
    scenario = Scenario('simple')
    scenario.steps['stepA'].run = MagicMock(side_effect=IndexError)
    scenario.run('stepA')
    assert "Run crash, please check previous message" in caplog.text
    sys.exit.assert_called_once()
