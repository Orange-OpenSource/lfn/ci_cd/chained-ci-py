FROM python:3.13.2-alpine@sha256:323a717dc4a010fee21e3f1aac738ee10bb485de4e7593ce242b36ee48d6b352
LABEL maintainer="David Blaisonneau <david.blaisonneau@orange.com>"

ARG VCS_REF
ARG USER=user
ARG SRC=/usr/src/app/chained-ci-py

LABEL org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.vcs-url="https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci-py" \
      org.label-schema.description="chained-ci-py Docker image"

ENV HOME /home/$USER

# renovate: datasource=repology depName=alpine_3_21/py3-pip versioning=loose
ENV PY3_PIP_VERSION=24.3.1-r0
# renovate: datasource=repology depName=alpine_3_21/python3-dev versioning=loose
ENV PYTHON3_DEV_VERSION=3.12.9-r0
# renovate: datasource=repology depName=alpine_3_21/build-base versioning=loose
ENV BUILD_BASE_VERSION=0.5-r3
# renovate: datasource=repology depName=alpine_3_21/libffi-dev versioning=loose
ENV LIBFFI_DEV_VERSION=3.4.6-r0
# renovate: datasource=repology depName=alpine_3_21/openssl-dev versioning=loose
ENV OPENSSL_DEV_VERSION=3.3.3-r0
# renovate: datasource=repology depName=alpine_3_21/git versioning=loose
ENV GIT_VERSION=2.47.2-r0
# renovate: datasource=repology depName=alpine_3_21/libexpat versioning=loose
ENV LIBEXPAT_VERSION=2.6.4-r0
# renovate: datasource=repology depName=alpine_3_21/busybox versioning=loose
ENV BUSYBOX_VERSION=1.37.0-r12
# renovate: datasource=repology depName=alpine_3_21/busybox-binsh versioning=loose
ENV BUSYBOX_BINSH_VERSION=1.37.0-r12
# renovate: datasource=repology depName=alpine_3_21/ssl_client versioning=loose
ENV SSL_CLIENT_VERSION=1.37.0-r12

COPY chainedci $SRC/chainedci
COPY setup.* requirements.txt README.md MANIFEST.in $SRC/

WORKDIR $SRC
RUN adduser -D $USER -u 1001 &&\
      apk --no-cache --update-cache add --virtual build \
            build-base="${BUILD_BASE_VERSION}" \
            busybox="${BUSYBOX_VERSION}" \
            busybox-binsh="${BUSYBOX_BINSH_VERSION}" \
            libexpat="${LIBEXPAT_VERSION}" \
            libffi-dev="${LIBFFI_DEV_VERSION}" \
            openssl-dev="${OPENSSL_DEV_VERSION}"\
            python3-dev="${PYTHON3_DEV_VERSION}" \
            py3-pip="${PY3_PIP_VERSION}" \
            ssl_client="${SSL_CLIENT_VERSION}" \
            && \
      apk --no-cache --update-cache add \
            git="${GIT_VERSION}" && \
      pip install --no-cache-dir -r requirements.txt && \
      python setup.py build --executable /usr/local/bin/python &&\
      python setup.py install &&\
      rm -rf /usr/local/bin/ansible* && \
      apk del build &&\
      rm -rf /var/lib/apt/lists/* /tmp/* &&\
      rm -rf ~/.cache/pip

USER $USER
WORKDIR $HOME
CMD ["chainedci"]
